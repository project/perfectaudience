
jQuery(document).ready(function() {

  window._pa = window._pa || {};
  if (typeof Drupal.settings.perfectaudience.orderId != 'undefined') {
    _pa.orderId = Drupal.settings.perfectaudience.orderId;
  }
  if (typeof Drupal.settings.perfectaudience.revenue != 'undefined') {
    _pa.revenue = Drupal.settings.perfectaudience.revenue;
  }
  if (typeof Drupal.settings.perfectaudience.onLoadEvent != 'undefined') {
    _pa.onLoadEvent = Drupal.settings.perfectaudience.onLoadEvent;
  }

  var pa = document.createElement('script');
  pa.type = Drupal.settings.perfectaudience.type;
  pa.async =  Drupal.settings.perfectaudience.async;
  pa.src = ('https:' == document.location.protocol ? 'https:' : 'http:') + Drupal.settings.perfectaudience.src;

  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(pa, s);

});
