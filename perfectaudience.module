<?php

/*
 * @file
 * Drupal Module: Perfect Audience Retargeting
 * Adds the required Javascript to all of your Drupal pages
 * to allow tracking by the Perfect Audience Retargeting statistics package.
 *
 * @author: Kai Curry <http://drupal.org/user/90662>
 */

/**
 * Implements hook_help().
 */
function perfectaudience_help($path, $arg) {
  switch ($path) {
    case 'admin/settings/perfectaudience':
      return t('<a href="@pa_url">Perfect Audience</a> is a retargeting too that lets marketers bring back lost web visitors with ads across the web.', array('@pa_url' => 'https://www.perfectaudience.com'));
  }
}

/**
 * Implements hook_perm().
 */
function perfectaudience_perm() {
  $perms = array(
    'administer perfectaudience',
  );
  return $perms;
}

/**
 * Implementats hook_menu().
 */
function perfectaudience_menu() {
  $items['admin/settings/perfectaudience'] = array(
    'title' => 'Perfect Audience Retargeting',
    'description' => 'Configure the settings used to generate your Perfect Audience Retargeting tracking code.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('perfectaudience_admin_settings_form'),
    'access arguments' => array('administer perfectaudience'),
    'file' => 'perfectaudience.admin.inc',
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

/**
 * Implements hook_init().
 */
function perfectaudience_init() {
  global $user;

  $id = variable_get('perfectaudience_site_id', '');

  // 1. Check if the id has a value.
  // 2. Track page views based on visibility value.
  // 3. Check if we should track the currently active user's role.
  if (!empty($id) && _perfectaudience_visibility_pages() && _perfectaudience_visibility_user($user)) {

    $settings = array(
      'type' => 'text/javascript',
      'async' => TRUE,
      'src' => '//tag.perfectaudience.com/serve/' . $id . '.js',
    );

    // Add extra tracking.
    $track_order = variable_get('perfectaudience_track_order_id', 1);
    $track_revenue = variable_get('perfectaudience_track_revenue', 1);

    if ($track_order || $track_revenue) {
      if ($order_id = perfectaudience_uc_order_id()) {
        // OPTIONAL: include your user's email address or order ID
        if ($track_order) {
          if (variable_get('perfectaudience_order_id_email', 0)) {
            $settings['orderId'] = $user->mail;
          }
          else {
            $settings['orderId'] = $order_id;
          }
        }
        // OPTIONAL: include dynamic purchase value for the conversion
        if ($track_revenue) $settings['revenue'] = _perfectaudience_uc_order_get_total($order_id); //uc_order_total($order_id);
      }
    }

    // OPTIONAL: name of segment/conversion to be fired on script load

    if (!empty($settings)) {
      drupal_add_js(array('perfectaudience' => $settings), 'setting', 'header');
      drupal_add_js(drupal_get_path('module', 'perfectaudience') .'/perfectaudience.js', 'module', 'header');
    }
  }
}

/**
 * Based on visibility setting this function returns TRUE if code should
 * be added to the current page and otherwise FALSE.
 */
function _perfectaudience_visibility_pages() {
  static $page_match;

  // Cache visibility setting in hook_init for hook_footer.
  if (!isset($page_match)) {

    $visibility = variable_get('perfectaudience_visibility', 0);

    // Remove tracking from all administrative pages.
    $no_track_pages = "admin\nadmin/*\nbatch\nnode/add*\nnode/*/*\nuser/*/*";
    $setting_pages = variable_get('perfectaudience_pages', $no_track_pages);

    // Match path if necessary.
    if (!empty($setting_pages)) {
      // Convert path to lowercase. This allows comparison of the same path
      // with different case. Ex: /Page, /page, /PAGE.
      $pages = drupal_strtolower($setting_pages);
      if ($visibility < 2) {
        // Convert the Drupal path to lowercase
        $path = drupal_strtolower(drupal_get_path_alias($_GET['q']));
        // Compare the lowercase internal and lowercase path alias (if any).
        $page_match = drupal_match_path($path, $pages);
        if ($path != $_GET['q']) {
          $page_match = $page_match || drupal_match_path($_GET['q'], $pages);
        }
        // When $visibility has a value of 0, the tracking code is displayed on
        // all pages except those listed in $pages. When set to 1, it
        // is displayed only on those pages listed in $pages.
        $page_match = !($visibility xor $page_match);
      }
      else {
        $page_match = drupal_eval($setting_pages);
      }
    }
    else {
      $page_match = TRUE;
    }

  }
  return $page_match;
}

/**
 * Tracking visibility check for an user object.
 *
 * @param $account
 *   A user object containing an array of roles to check.
 * @return boolean
 *   A decision on if the current user is being tracked.
 */
function _perfectaudience_visibility_user($account) {

  $enabled = FALSE;

  // Is current user a member of a role that should be tracked?
  $enabled = _perfectaudience_visibility_roles($account);

  return $enabled;
}

/**
 * Based on visibility setting this function returns TRUE if code should
 * be added for the current role and otherwise FALSE.
 */
function _perfectaudience_visibility_roles($account) {

  $visibility = variable_get('perfectaudience_visibility_roles', 0);
  $enabled = $visibility;
  $roles = variable_get('perfectaudience_roles', array());

  if (array_sum($roles) > 0) {
    // One or more roles are selected.
    foreach (array_keys($account->roles) as $rid) {
      // Is the current user a member of one of these roles?
      if (isset($roles[$rid]) && $rid == $roles[$rid]) {
        // Current user is a member of a role that should be tracked/excluded from tracking.
        $enabled = !$visibility;
        break;
      }
    }
  }
  else {
    // No role is selected for tracking, therefore all roles should be tracked.
    $enabled = TRUE;
  }

  return $enabled;
}

/**
 * Look to see if this is a checkout confirm page.
 * Return order_id if the url is cart/checkout/complete.
 */
function perfectaudience_uc_order_id() {
  $order_id = FALSE;
  if (module_exists('uc_order') && 'cart' == arg(0) && 'checkout' == arg(1) && 'complete' == arg(2)) {
    if (isset($_SESSION['cart_order']) && $_SESSION['cart_order']) {
      $order_id = $_SESSION['cart_order'];
    }
  }
  return $order_id;
}

/**
 * Load the uc_order and get the total.
 */
function _perfectaudience_uc_order_get_total($order_id) {
  $order = uc_order_load($order_id);
  $order_total = uc_order_get_total($order);
  return $order_total;
}
