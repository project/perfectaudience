<?php

/**
 * @file
 * Administrative page callbacks for the perfectaudience module.
 */

/**
 * Implements hook_admin_settings() for configuring the module
 */
function perfectaudience_admin_settings_form(&$form_state) {
  $form['account'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
    '#collapsible' => FALSE,
  );

  $form['account']['perfectaudience_site_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Site ID'),
    '#default_value' => variable_get('perfectaudience_site_id', ''),
    '#size' => 20,
    '#maxlength' => 25,
    '#required' => TRUE,
    '#description' => t('This ID is unique to each site you want to track. The "Site ID" can be found in your Perfect Audience dashboard under "Manage" -> "Site Tracking Tag."', array()),
  );

  return system_settings_form($form);
}
